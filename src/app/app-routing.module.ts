// ANGULAR
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// HOME
import { HomeNotLoggedComponent } from './home-not-logged/home-not-logged.component';
import { TradingComponent } from '../app/home/trading/trading.component';
import { CandlechartComponent } from '../app/home/trading/candlechart/candlechart.component';
import { OperationsSideComponent } from '../app/home/operations-side/operations-side.component';
import { DepthchartComponent } from '../app/home/trading/depthchart/depthchart.component';
// OTHER
import { LandingpageComponent } from './landingpage/landingpage.component';
import { AboutUsComponent } from './landingpage/about-us/about-us.component';
import { JobsComponent } from './jobs/jobs.component';
import { VacancyComponent } from './jobs/vacancy/vacancy.component';
import { HowItWorksComponent } from './landingpage/how-it-works/how-it-works.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';

const appRoutes: Routes = [
    { path: '', pathMatch: 'full', component: LandingpageComponent },
    { path: 'como-funciona', component: HowItWorksComponent },
    { path: 'sobre', component: AboutUsComponent },
    { path: 'vagas', component: JobsComponent },
    { path: 'vagas/:id', component: VacancyComponent },
    { path: 'termos-e-condicoes-de-uso', component: TermsComponent },
    { path: 'politicas-de-privacidade', component: PrivacyPolicyComponent },
    {
        path: 'exchange', component: HomeNotLoggedComponent, children: [
            { path: 'trade', component: TradingComponent },
            { path: 'candlechart', component: CandlechartComponent },
            { path: 'operations', component: OperationsSideComponent },
            { path: 'depthchart', component: DepthchartComponent },
        ]
    },
    { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers: []
})

export class AppRoutingModule { }
