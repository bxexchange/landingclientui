// ANGULAR
import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { registerLocaleData, DatePipe } from '@angular/common';
import localePt from '@angular/common/locales/pt';
// NGX
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { ptBrLocale } from 'ngx-bootstrap/locale';
defineLocale('pt-br', ptBrLocale);
// ANGULAR MATERIAL
import 'hammerjs';
import { MatRadioModule } from '@angular/material/radio';
import { MatInputModule } from '@angular/material/input';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatListModule } from '@angular/material/list';
import {
    MatIconModule, MatSelectModule, MatCheckboxModule, MatNativeDateModule,
    MAT_DIALOG_DEFAULT_OPTIONS, MatAutocompleteModule, MatFormFieldModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
// BASICS
import { AppRoutingModule } from './app-routing.module';
import { Auth0Service } from './auth/auth0.service';
import { NgReduxModule } from '@angular-redux/store';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { ClickOutsideModule } from 'ng4-click-outside';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { AmChartsModule } from '@amcharts/amcharts3-angular';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { appStoreProviders } from './app.store';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
// SERVICES
import { MktDataService } from '../api/mktdata.service';
// PIPES
import { ReplaceTwoDots } from './shared/pipes/replace-two-dots';
// MODALS
import { ErrorModalComponent } from './shared/modals/error-modal/error-modal.component';
// CREATED COMPONENTS
import { AppComponent } from './app.component';
import { LandingpageComponent } from './landingpage/landingpage.component';
import { RBookComponent } from './landingpage/r-book/r-book.component';
import { LBookComponent } from './landingpage/l-book/l-book.component';
import { LFooterComponent } from './shared/components/l-footer/l-footer.component';
import { HomeComponent } from './home/home.component';
import { HomeNotLoggedComponent } from './home-not-logged/home-not-logged.component';
import { TradingComponent } from './home/trading/trading.component';
import { BookComponent } from './home/trading/book/book.component';
import { TickerComponent } from './home/header/ticker/ticker.component';
import { SelectCryptoComponent } from './home/header/select-crypto/select-crypto.component';
import { CryptoTypeComponent } from './home/header/crypto-type/crypto-type.component';
import { CandlechartComponent } from './home/trading/candlechart/candlechart.component';
import { PricechartComponent } from './home/trading/pricechart/pricechart.component';
import { DepthchartComponent } from './home/trading/depthchart/depthchart.component';
import { QuotepanelComponent } from './home/trading/quotepanel/quotepanel.component';
import { OperationsSideComponent } from './home/operations-side/operations-side.component';
import { FooterComponent } from './home/footer/footer.component';
import { HeaderComponent } from './home/header/header.component';
import { NavbarComponent } from './shared/components/navbar/navbar.component';
import { HowItWorksComponent } from './landingpage/how-it-works/how-it-works.component';
import { AboutUsComponent } from './landingpage/about-us/about-us.component';
import { JobsComponent } from './jobs/jobs.component';
import { VacancyComponent } from './jobs/vacancy/vacancy.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

import { TermsComponent } from './terms/terms.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';

registerLocaleData(localePt);
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.lang.json');
}

@NgModule({
    declarations: [
        AppComponent,
        BookComponent,
        TickerComponent,
        SelectCryptoComponent,
        CryptoTypeComponent,
        FooterComponent,
        TradingComponent,
        LandingpageComponent,
        HeaderComponent,
        QuotepanelComponent,
        HomeComponent,
        OperationsSideComponent,
        DepthchartComponent,
        CandlechartComponent,
        PricechartComponent,
        ReplaceTwoDots,
        LBookComponent,
        RBookComponent,
        LFooterComponent,
        HowItWorksComponent,
        NavbarComponent,
        AboutUsComponent,
        JobsComponent,
        VacancyComponent,
        ErrorModalComponent,
        PageNotFoundComponent,
        HomeNotLoggedComponent,
        TermsComponent,
        PrivacyPolicyComponent,
    ],
    imports: [
        MatInputModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        HttpClientModule,
        NgReduxModule,
        CurrencyMaskModule,
        ClickOutsideModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        MatExpansionModule,
        MatRadioModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatSnackBarModule,
        MatDialogModule,
        MatTabsModule,
        MatPaginatorModule,
        MatTableModule,
        MatSortModule,
        MatListModule,
        MatIconModule,
        MatSelectModule,
        MatCheckboxModule,
        BrowserAnimationsModule,
        MatSlideToggleModule,
        NgIdleKeepaliveModule.forRoot(),
        AppRoutingModule,
        ReactiveFormsModule,
        AmChartsModule,
        ScrollToModule.forRoot(),
        MatAutocompleteModule,
        MatFormFieldModule,
        MatNativeDateModule,
        BsDatepickerModule.forRoot(),
        PopoverModule.forRoot(),
    ],
    providers: [
        MktDataService,
        Auth0Service,
        appStoreProviders,
        { provide: LOCALE_ID, useValue: 'pt-BR' },
        {
            provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {
                autoFocus: true,
                panelClass: 'custom-dialog',
                hasBackdrop: true,
                backdropClass: 'dark-backdrop'
            }
        },
        DatePipe,

    ],
    bootstrap: [AppComponent],
    schemas: [
        NO_ERRORS_SCHEMA,
    ],
    entryComponents: [
        ErrorModalComponent,
    ]
})
export class AppModule { }
