import { Component, EventEmitter, Output, Input, Inject } from '@angular/core';
import { BROKER_NAME } from '../../app.constants';
import { AppStore } from '../../app.store';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

    brokerName: string = BROKER_NAME;

    @Input() sidebarMode: string;
    @Output() changeToggle = new EventEmitter<any>();
    @Input() menuMobile: boolean;
    @Output() menuChange = new EventEmitter<any>();

    constructor() { }

    public toggleCollapseSidebar(): void {
        this.changeToggle.emit('toggle');
    }

    public toggleMenuMobile(): void {
        this.menuMobile = !this.menuMobile;
        this.menuChange.emit({ v: this.menuMobile });
    }
}

