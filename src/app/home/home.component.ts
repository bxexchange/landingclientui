import { Component, OnInit, Inject, ElementRef } from '@angular/core';
import { Auth0Service } from '../auth/auth0.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    public menuMobile;
    public sidebarMode = 'full';

    constructor(
        @Inject(Auth0Service) public auth: Auth0Service,
        private elementRef: ElementRef,
    ) { }

    ngOnInit() {
        localStorage.setItem('sidebarMode', 'full');
        this.elementRef.nativeElement.ownerDocument.body.style.overflow = 'hidden';
    }

    public toggleSidebar(): void {
        if (this.sidebarMode === 'full') {
            this.sidebarMode = 'collapse';
            localStorage.setItem('sidebarMode', 'collapse');
        } else {
            this.sidebarMode = 'full';
            localStorage.setItem('sidebarMode', 'full');
        }
    }

    public onMenuMobileEvent(evento): void {
        this.menuMobile = evento.v;
    }
}
