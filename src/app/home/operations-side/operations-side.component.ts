import { Component, OnInit, Inject } from '@angular/core';
import { AppStore } from '../../app.store';
import {
    TRADES_RENDER_INTERVAL,
} from '../../app.constants';

@Component({
    selector: 'app-operations-side',
    templateUrl: './operations-side.component.html',
    styleUrls: ['./operations-side.component.scss']
})
export class OperationsSideComponent implements OnInit {

    public operations;

    constructor(
        @Inject(AppStore) private store,

    ) {
        this.startUpdateOperations();
    }

    startUpdateOperations() {
        this.timer(this.updateOperations.bind(this), TRADES_RENDER_INTERVAL);
    }

    timer(callback, milliseconds) {
        setTimeout(() => {
            callback();
            this.timer(callback, milliseconds);
        }, milliseconds);
    }

    updateOperations() {
        const pair = this.store.getState().get('currentTicker').toJS().pair;

        const operations = this.store.getState().get('trades')
            .sortBy((trade) => trade.time)
            .toList()
            .toArray()
            .filter((trade) => trade.symbol === pair);

        this.operations = [];
        const lastPrice = 0;

        this.mountTrade(operations, this.operations, lastPrice);
    }

    mountTrade(operations, thisOperations, lastPrice) {
        let lastArrow = 'up';
        operations.forEach(element => {

            let arrow;

            Number(element.price) === lastPrice ? arrow = lastArrow : (element.price > lastPrice ? arrow = 'up' : arrow = 'down');

            lastArrow = arrow;
            lastPrice = element.price;

            thisOperations.unshift({
                size: element.size,
                price: Number(element.price).toFixed(2),
                arrow,
                time: element.time
            });
        });
    }

    ngOnInit() {
    }

    trackByFn(index, op) {
        return index;
    }
}
