import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PricechartComponent } from './pricechart.component';

describe('PricechartComponent', () => {
  let component: PricechartComponent;
  let fixture: ComponentFixture<PricechartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PricechartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PricechartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
