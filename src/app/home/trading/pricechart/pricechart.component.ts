import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { AmChart, AmChartsService, AmChartsModule, AmChartsDirective } from '@amcharts/amcharts3-angular';
import { AppStore } from '../../../app.store';
import { PRICECHART_RENDER_INTERVAL } from '../../../app.constants';
import { MatSelect, MatSelectChange } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-pricechart',
    templateUrl: './pricechart.component.html',
    styleUrls: ['./pricechart.component.scss']
})
export class PricechartComponent implements OnInit, OnDestroy {
    public chart: AmChart;
    public chartData = [];

    public counter;
    public timeRef;
    public chartPeriod = 604800000; // 1 week = 604800000ms

    public loggedIn = false;

    public timeOptions = ['1m', '3m', '5m', '30m', '1h', '2h', '4h', '6h', '12h', '1d'];
    public timeSelected = '1m';

    public generatedHistory = false;

    private translations;

    constructor(
        private AmCharts: AmChartsService,
        private translate: TranslateService,
        @Inject(AppStore) private store,
    ) {
        store.subscribe(() => {
            this.updateState();
        });
    }

    ngOnDestroy() {
        if (this.chart) {
            this.AmCharts.destroyChart(this.chart);
        }
        clearTimeout(this.counter);
    }

    ngOnInit() {
        const state = this.store.getState().toJS();

        this.createChartOptions();
        this.generateHistoryChart(60000); // period of 1 min = 60000 ms
        this.startUpdateChart();
        this.loggedIn = this.store.getState().get('userStatus').toJS().loggedIn;
    }

    public updateState(): void {
        this.loggedIn = this.store.getState().get('userStatus').toJS().loggedIn;
    }

    // adjust update around every HH:mm:05 instant
    public startUpdateChart() {
        if ((new Date()).getTime() % 60000 < 5000) {
            this.timer((new Date()).getTime() % 60000 < 5000)
        } else {
            this.timer(65000 - (new Date()).getTime() % 60000);
        }
    }

    public timer(milliseconds) {
        this.counter = setTimeout(() => {
            this.timer(PRICECHART_RENDER_INTERVAL);
            if (this.generatedHistory) {
                this.updateChart();
            } else {
                this.generateHistoryChart(60000);
            }
        }, milliseconds);
    }

    public generateHistoryChart(period) {
        const pair = this.store.getState().get('currentTicker').toJS().pair;
        const now = (new Date()).getTime();

        // filter data to get trades of last week
        const operations = this.store.getState().get('trades')
            .sortBy((trade) => trade.time)
            .toList()
            .toArray()
            .filter((trade) => trade.symbol === pair
                && trade.time > now - this.chartPeriod);


        // if didn't find a history of operations get now as time reference
        if (operations.length === 0) {
            const iArrayFullDate = new Date();
            this.timeRef = (new Date(iArrayFullDate.getFullYear(), iArrayFullDate.getMonth(), iArrayFullDate.getDate(), iArrayFullDate.getHours())).getTime();
            while (this.timeRef < now) {
                this.timeRef += period;
            }

            this.translate.get(['loadingChartData',
                                'price'
                ]).subscribe(translations => {
                    this.translations = translations;

                    // add label
                    this.chart.addLabel(0, '50%', this.translations.loadingChartData, 'center');
                    // set opacity of the chart div
                    this.chart.chartDiv.style.opacity = 0.5;
                    // redraw it
                    this.chart.validateNow();
                }
            );

            return;
            // if found a history get the day and hours of first trade time as reference
        } else {
            const iArrayFullDate = new Date(operations[0].time);
            this.timeRef = (new Date(iArrayFullDate.getFullYear(), iArrayFullDate.getMonth(), iArrayFullDate.getDate(), iArrayFullDate.getHours())).getTime();
            this.chart.clearLabels();
            this.chart.chartDiv.style.opacity = 1;
            this.chart.validateNow();
        }

        this.chartData = [];
        this.generatedHistory = true;

        // adjust minutes of time reference
        let lastOperation = operations[0];
        while (this.timeRef < lastOperation.time) {
            this.timeRef += period;
        }

        // set data chart filling periods without trades with price of last trade
        operations.forEach(operation => {
            while (operation.time > this.timeRef) {
                this.chartData.push({
                    date: this.timeRef,
                    price: Number(lastOperation.price).toFixed(2)
                });
                this.timeRef += period;
            }
            lastOperation = operation;
        });

        // fill from last operation found untill now if needded
        while (this.timeRef < now) {
            this.chartData.push({
                date: this.timeRef,
                price: Number(lastOperation.price).toFixed(2)
            })
            this.timeRef += period;
        }

        if (this.chart) {
            this.chart.dataProvider = this.chartData;
            this.chart.validateData();
        }
    }

    public updateChart() {
        const pair = this.store.getState().get('currentTicker').toJS().pair;
        const now = (new Date()).getTime();
        let newOperations;

        // filter operations for last period of 1m
        newOperations = this.store.getState().get('trades')
            .sortBy((trade) => trade.time)
            .toList()
            .toArray()
            .filter((trade) => trade.symbol === pair
                && trade.time >= this.timeRef - 60000
                && trade.time < this.timeRef
            );

        // if first data is out of 1 week interval remove from graph
        if (this.chartData.length > 0 && this.chartData[0].time < this.timeRef - this.chartPeriod) {
            this.chartData.splice(0, 1);
        }

        // if it doesn't have any trade between last chart update and now
        // copy last price
        if (newOperations.length === 0) {
            if (this.chart && this.chartData.length > 0 && this.chart) {
                const operation = this.chartData[this.chartData.length - 1];
                this.chartData.push({
                    date: this.timeRef,
                    price: Number(operation.price).toFixed(2)
                });
                this.chart.dataProvider = this.chartData;
                this.chart.validateData();
                this.timeRef += 60000;

                if (this.chart.endIndex === this.chartData.length - 2) {
                    this.chart.zoomToIndexes(this.chart.startIndex + 1, this.chart.endIndex + 1);
                }
            }
            return;
        }

        // if it has a trade add the last one to chart
        if (this.chart) {
            let lastOperation = newOperations[0];
            newOperations.forEach(operation => {
                if (operation.time > this.timeRef) {
                    this.chartData.push({
                        date: this.timeRef,
                        price: Number(lastOperation.price).toFixed(2)
                    });
                } else if (operation === newOperations[newOperations.length - 1]) {
                    this.chartData.push({
                        date: this.timeRef,
                        price: Number(operation.price).toFixed(2)
                    });
                }
                lastOperation = operation;
            });
            this.chart.dataProvider = this.chartData;
            this.chart.validateData();
            this.timeRef += 60000;

            if (this.chart.endIndex === this.chartData.length - 2) {
                this.chart.zoomToIndexes(this.chart.startIndex + 1, this.chart.endIndex + 1);
            }
        }
    }

    public createChartOptions(): void {
        this.chart = this.AmCharts.makeChart('pricechartdiv', {
            'type': 'serial',
            'theme': 'dark',
            'marginRight': 80,
            'hideCredits': true,
            'dataProvider': this.chartData,
            'categoryField': 'date',
            'fontFamily': 'Montserrat',
            'decimalSeparator': ',',
            'thousandsSeparator': '.',
            'language': 'pt',
            'mouseWheelZoomEnabled': true,
            'zoomOutOnDataUpdate': false,
            'graphs': [
                {
                    'id': 'g1',
                    'useNegativeColorIfDown': true,
                    'bullet': 'diamond',
                    'maxBulletSize': 10,
                    'hideBulletsCount': 20,
                    'lineAlpha': 1,
                    'lineThickness': 2,
                    'lineColor': '#4dde90',
                    'negativeLineColor': '#ff4823',
                    'valueField': 'price',
                    'precision': 2,
                    'balloonText':
                        `Preço: <b>R$ [[value]]</b>
                        Data: <b>[[category]]<b>
                        `,
                }
            ],
            'balloon': {
                'adjustBorderColor': true,
                'cornerRadius': 5,
                'fontSize': 12,
                'dateFormate': true,
            },
            'chartCursor': {
                'pan': true,
                'valueLineEnabled': false,
                'valueLineBalloonEnabled': false,
                'categoryLineEnabled': true,
                'categoryBalloonEnabled': false,
                'categoryBalloonDateFormat': 'DD MMM JJ:NNh',
            },
            'chartScrollbar': {
                'enabled': true,
                'scrollbarHeight': 10,
                'backgroundAlpha': 0.1,
                'backgroundColor': '#555',
                'selectedBackgroundColor': '#999',
                'selectedBackgroundAlpha': 1,
                'dragIcon': 'dragIconRectSmall',
                'dragIconHeight': 25,
                'dragIconWidth': 0,
                'oppositeAxis': false,
                'hideResizeGrips': true,
                'offset': 25
            },
            'categoryAxis': {
                'parseDates': true,
                'equalSpacing': false,
                'minVerticalGap': 20,
                'minPeriod': 'mm',
                'gridAlpha': 0,
                'tickLength': 0,
                'axisAlpha': 0
            },
            'valueAxes': [
                {
                    'position': 'right',
                    // 'title': this.translations.price,
                    'title': 'Preço'
                }
            ]
        });
    }

    public selectFilterTime(matSelectChange: any) {
        this.timeSelected = matSelectChange.value;
        clearTimeout(this.counter);
        switch (matSelectChange.value) {
            case '1d':
                // 1 day = 86400000 milliseconds
                this.generateHistoryChart(86400000);
                break;
            case '12h':
                // 12 hours = 43200000 milliseconds
                this.generateHistoryChart(43200000);
                break;
            case '6h':
                // 6 hours = 21600000 milliseconds
                this.generateHistoryChart(21600000);
                break;
            case '4h':
                // 4 hours = 14400000 milliseconds
                this.generateHistoryChart(14400000);
                break;
            case '2h':
                // 2 hours = 7200000 milliseconds;
                this.generateHistoryChart(7200000);
                break;
            case '1h':
                // 1 hour = 3600000 milliseconds
                this.generateHistoryChart(3600000);
                break;
            case '30m':
                // 30 minutes = 1800000 milliseconds
                this.generateHistoryChart(1800000);
                break;
            case '15m':
                // 15 minutes = 900000 milliseconds
                this.generateHistoryChart(900000);
                break;
            case '5m':
                // 5 minutes = 300000 milliseconds
                this.generateHistoryChart(300000);
                break;
            case '3m':
                // 3 minutes = 180000 milliseconds
                this.generateHistoryChart(180000);
                break;
            case '1m':
                // 1 minute = 60000 milliseconds
                this.generateHistoryChart(60000);
                this.startUpdateChart();
                break;
            default:
                break;
        }
    }
}
