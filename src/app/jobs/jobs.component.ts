import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-jobs',
    templateUrl: './jobs.component.html',
    styleUrls: ['./jobs.component.scss']
})
export class JobsComponent implements OnInit {
    public all: boolean;
    public dev: boolean;
    public mkt: boolean;
    public sup: boolean;

    public id: number;

    public vacancys = [];

    // public vacancys = [
    //     {
    //         id: 1,
    //         name: 'Analista de Sistemas',
    //         city: 'São Paulo',
    //         country: 'Brasil'
    //     },
    //     {
    //         id: 2,
    //         name: 'Vendedor',
    //         city: 'São Paulo',
    //         country: 'Brasil'
    //     },
    // ];

    constructor(
        private router: Router,
    ) { }

    ngOnInit() {
        this.all = true;
        this.dev = false;
        this.mkt = false;
        this.sup = false;

        this.id = 0;
    }

    public filter(filter: number): void {
        if (filter === 1) {
            this.all = true;
            this.dev = false;
            this.mkt = false;
            this.sup = false;
        } else if (filter === 2) {
            this.all = false;
            this.dev = true;
            this.mkt = false;
            this.sup = false;
        } else if (filter === 3) {
            this.all = false;
            this.dev = false;
            this.mkt = true;
            this.sup = false;
        } else if (filter === 4) {
            this.all = false;
            this.dev = false;
            this.mkt = false;
            this.sup = true;
        }
    }

    public loadVacancy(id: number) {
        this.router.navigate(['vagas', id]);
    }

}
