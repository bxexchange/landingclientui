import { Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';


@Component({
    selector: 'app-l-footer',
    templateUrl: './l-footer.component.html',
    styleUrls: ['./l-footer.component.scss']
})
export class LFooterComponent implements OnInit {

    public appURL;

    constructor(
    ) { }

    ngOnInit () {
        this.appURL = environment.signin_url;
    }

}
