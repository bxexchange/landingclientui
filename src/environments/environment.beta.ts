export const environment = {
    production: true,
    broker: {
        name: 'ABAKATE'
    },
    ws_mktdata: 'wss://mktdata.nva.h3.exchange',
    ws_trade: 'wss://trading.nva.h3.exchange',
    signin_url: 'https://app.beta.abakate.com.br/',
    signup_url: 'https://app.beta.abakate.com.br/pre-cadastro/cupom',
    envName: 'beta',
    Auth0: {
        allowSignUp: false,
        clientId: 'zVZv3Fv01bMEZX70RQf5IRrDVezAhs3j',
        domain: 'h3-abakate-beta.auth0.com',
        responseType: 'token id_token',
        audience: 'https://h3-abakate-beta.auth0.com/userinfo',
        redirectUri: 'https://app.beta.abakate.com.br/home/trade',
        scope: 'openid profile ', // Learn about scopes: https://auth0.com/docs/scopes,
    },
};
